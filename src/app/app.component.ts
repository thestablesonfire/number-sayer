import { Component } from '@angular/core';
import { NumberSayer } from './NumberSayer/number-sayer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  numberInput = 1;

  talker: NumberSayer;
  numberOutput: string;

  constructor() {
    this.talker = new NumberSayer();
    this.numberOutput = this.talker.sayIt(this.numberInput || 0);
  }

  convertToEnglish() {
    this.numberOutput = this.talker.sayIt(this.numberInput || 0);
  }
}
