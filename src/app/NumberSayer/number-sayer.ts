import {NumberStringFactory} from '../NumberStringFactory/number-string-factory';
import {InputNumber} from '../InputNumber/input-number';

export class NumberSayer {
    /**
     * A counter needed to hold the index of the current digit in a triple
     */
    private tripleCounter: number;

    /**
     * Returns the correct string for a power of one hundred
     * @param digit
     * @returns {string}
     */
    private static addPowerOfOneHundred(digit: number) {
        return NumberStringFactory.getDigit(digit) + ' Hundred';
    }

    /**
     * Adds the power of one thousand for the corresponding triple position (if applicable)
     * @param index
     * @param inputNumber
     * @param english
     */
    private static addPowerOfOneThousand(index: number, inputNumber: InputNumber, english: string[]) {
        const ten = NumberStringFactory.getPowerOfOneThousand(inputNumber.getNumTriples() - (index + 1));

        if (ten) {
            english.push(ten);
        }
    }

    /**
     * Converts the array of triples to english
     * @param inputNumber
     * @returns {string}
     */
    private convertArrayToEnglish(inputNumber: InputNumber) {
        const english = [];

        // For each triple: convert it to english, if it requires a string to express, push it and add the power of
        // one thousand (if applicable)
        for (let i = 0; i < inputNumber.getNumTriples(); i++) {
            const triple = this.convertTripleToEnglish(inputNumber.getInputAt(i));

            if (triple) {
                english.push(triple);
                NumberSayer.addPowerOfOneThousand(i, inputNumber, english);
            }
        }

        return english.join(' ');
    }

    /**
     * Converts a single triple to an english string
     * @param triple
     * @returns {string}
     */
    private convertTripleToEnglish(triple: string) {
        const english = [];

        // For the length of the triple get a string for each digit
        for (this.tripleCounter = 0; this.tripleCounter < 3; this.tripleCounter++) {
            const digit = parseInt(triple[this.tripleCounter], 10);

            // If the triple is all zeroes, don't push anything.
            // There is no data to display outside the power of 1000
            if (digit) {
                english.push(this.stringForPosition(digit, triple));
            }
        }

        // Return the full triple string or null if not needed
        return english.length ? english.join(' ') : null;
    }

    /**
     * Returns the correct string for a power of ten
     * @param digit
     * @param triple
     * @returns {any}
     */
    private addPowerOfTen(digit: number, triple: string) {
        // If the digit is a 1, it is some sort of 'teen', otherwise it is a standard power of ten
        let val = '';

        if (digit === 1) {
            this.tripleCounter++;
            val = NumberStringFactory.getTeen(triple);
        } else {
            val = NumberStringFactory.getTen(digit);
        }

        return val;
    }

    /**
     * Returns the string needed for the appropriate digit and position in a triple
     * @param digit
     * @param triple
     * @returns {string}
     */
    private stringForPosition(digit: number, triple: string) {
        let english = '';

        switch (this.tripleCounter) {
            case 0:
                english = NumberSayer.addPowerOfOneHundred(digit);
            break;
            case 1:
                english = this.addPowerOfTen(digit, triple);
            break;
            default:
                english = NumberStringFactory.getDigit(digit);
            break;
        }

        return english;
    }

    /**
     * Top level function that converts the input to an array, then returns the input in English
     * @param {number} input
     * @returns {string}
     */
    public sayIt(input: number) {
        const inputNumber = new InputNumber(input);

        return this.convertArrayToEnglish(inputNumber);
    }
}
