import {PowersOfOneThousand} from '../enums/powers-of-one-thousand.enum';
import {Tens} from '../enums/tens.enum';
import {Teens} from '../enums/teens.enum';
import {SingleDigits} from '../enums/single-digits.enum';

export class NumberStringFactory {
    /**
     * Converts a numerical digit to the English string for that digit
     * @param digit
     * @returns {any}
     */
    public static getDigit(digit: number) {
        return SingleDigits[digit];
    }

    /**
     * Converts a numerical digit to the English string for that power of ten
     * @param digit
     * @returns {any}
     */
    public static getTen(digit: number) {
        return Tens[digit - 2];
    }

    /**
     * Converts an index to the corresponding power of one thousand
     * @param index
     * @returns {any}
     */
    public static getPowerOfOneThousand(index: number) {
        return PowersOfOneThousand[index];
    }

    /**
     * If the digit is for some sort of teen, we need to increase the counter to skip the single digit, because that
     * is accounted for by the teen
     * @param triple
     * @returns {any}
     */
    public static getTeen(triple: string) {
        return Teens[triple[2]];
    }
}
