export class InputNumber {
    number: string[];

    /**
     * Takes in a number string and slices it up into triples and adds those triples to an array
     * @param number
     * @returns {Array}
     */
    private static groupToArray(number: number) {
        const arr = [];
        let numberStr = number.toString(10);

        while (numberStr.length) {
            arr.unshift(numberStr.length >= 3 ? numberStr.slice(numberStr.length - 3) : InputNumber.padTriple(numberStr));
            numberStr = numberStr.slice(0, -3);
        }

        return arr;
    }

    /**
     * Pads a number string to be 3 chars long
     * @param num
     * @returns {any}
     */
    private static padTriple(num: string) {
        const zero = '0';

        while (num.length < 3) {
            num = zero + num;
        }

        return num;
    }

    /**
     * Creates the number array on creation
     * @param input
     */
    constructor(input) {
        this.number = InputNumber.groupToArray(input);
    }

    /**
     * Returns the number of triples in the input array
     * @returns {number}
     */
    public getNumTriples() {
        return this.number.length;
    }

    /**
     * Returns the input triple at i index
     * @param i
     * @returns {string}
     */
    public getInputAt(i) {
        return this.number[i];
    }

    /**
     * Returns the whole triple array
     * @returns {string[]}
     */
    public getInput() {
        return this.number;
    }
}
