export enum PowersOfOneThousand {
    '',
    'Thousand',
    'Million',
    'Billion',
    'Trillion',
    'Quadrillion'
}
