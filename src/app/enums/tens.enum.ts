export enum Tens {
    'Twenty',
    'Thirty',
    'Fourty',
    'Fifty',
    'Sixty',
    'Seventy',
    'Eighty',
    'Ninety'
}
