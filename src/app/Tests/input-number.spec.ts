import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from '../app.component';
import { FormsModule } from '@angular/forms';
import { InputNumber } from '../InputNumber/input-number';

describe('InputNumber', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                FormsModule
            ]
        }).compileComponents();
    }));

    it('should create the class', async(() => {
        const sayer = new InputNumber(123);
        expect(sayer).toBeTruthy();
    }));

    it('should group the numbers to an array correctly', async(() => {
        expect(new InputNumber(1).getInput()).toEqual(['001']);
        expect(new InputNumber(12).getInput()).toEqual(['012']);
        expect(new InputNumber(123).getInput()).toEqual(['123']);

        expect(new InputNumber(1123).getInput()).toEqual(['001', '123']);
        expect(new InputNumber(20123).getInput()).toEqual(['020', '123']);
        expect(new InputNumber(332123).getInput()).toEqual(['332', '123']);
    }));

    it('should return the correct values', async(() => {
        const inputNumber = new InputNumber(112200499238);

        expect(inputNumber.getInput()).toEqual(['112', '200', '499', '238']);
        expect(inputNumber.getNumTriples()).toEqual(4);
        expect(inputNumber.getInputAt(1)).toEqual('200');
    }));
});
