import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from '../app.component';
import { FormsModule } from '@angular/forms';
import { NumberStringFactory } from '../NumberStringFactory/number-string-factory';

describe('NumberStringFactory', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                FormsModule
            ]
        }).compileComponents();
    }));

    it('should create the class', async(() => {
        const numFactory = new NumberStringFactory();
        expect(numFactory).toBeTruthy();
    }));

    it('should return the correct single digits', async(() => {
        expect(NumberStringFactory.getDigit(0)).toBe('Zero');
        expect(NumberStringFactory.getDigit(1)).toBe('One');
        expect(NumberStringFactory.getDigit(2)).toBe('Two');
        expect(NumberStringFactory.getDigit(3)).toBe('Three');
        expect(NumberStringFactory.getDigit(4)).toBe('Four');
        expect(NumberStringFactory.getDigit(5)).toBe('Five');
        expect(NumberStringFactory.getDigit(6)).toBe('Six');
        expect(NumberStringFactory.getDigit(7)).toBe('Seven');
        expect(NumberStringFactory.getDigit(8)).toBe('Eight');
        expect(NumberStringFactory.getDigit(9)).toBe('Nine');
    }));

    it('should return the correct teens', async(() => {
        expect(NumberStringFactory.getTeen('010')).toBe('Ten');
        expect(NumberStringFactory.getTeen('111')).toBe('Eleven');
        expect(NumberStringFactory.getTeen('212')).toBe('Twelve');
        expect(NumberStringFactory.getTeen('313')).toBe('Thirteen');
        expect(NumberStringFactory.getTeen('414')).toBe('Fourteen');
        expect(NumberStringFactory.getTeen('515')).toBe('Fifteen');
        expect(NumberStringFactory.getTeen('616')).toBe('Sixteen');
        expect(NumberStringFactory.getTeen('717')).toBe('Seventeen');
        expect(NumberStringFactory.getTeen('818')).toBe('Eighteen');
        expect(NumberStringFactory.getTeen('919')).toBe('Nineteen');
    }));

    it('should return the correct tens', async(() => {
        expect(NumberStringFactory.getTen(2)).toBe('Twenty');
        expect(NumberStringFactory.getTen(3)).toBe('Thirty');
        expect(NumberStringFactory.getTen(4)).toBe('Fourty');
        expect(NumberStringFactory.getTen(5)).toBe('Fifty');
        expect(NumberStringFactory.getTen(6)).toBe('Sixty');
        expect(NumberStringFactory.getTen(7)).toBe('Seventy');
        expect(NumberStringFactory.getTen(8)).toBe('Eighty');
        expect(NumberStringFactory.getTen(9)).toBe('Ninety');
    }));

    it('should return the correct power of 1000', async(() => {
        expect(NumberStringFactory.getPowerOfOneThousand(0)).toBe('');
        expect(NumberStringFactory.getPowerOfOneThousand(1)).toBe('Thousand');
        expect(NumberStringFactory.getPowerOfOneThousand(2)).toBe('Million');
        expect(NumberStringFactory.getPowerOfOneThousand(3)).toBe('Billion');
        expect(NumberStringFactory.getPowerOfOneThousand(4)).toBe('Trillion');
        expect(NumberStringFactory.getPowerOfOneThousand(5)).toBe('Quadrillion');
    }));
});
