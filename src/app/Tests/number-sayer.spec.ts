import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from '../app.component';
import { FormsModule } from '@angular/forms';
import { NumberSayer } from '../NumberSayer/number-sayer';

describe('NumberSayer', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                FormsModule
            ]
        }).compileComponents();
    }));

    it('should create the class', async(() => {
        const sayer = new NumberSayer();
        expect(sayer).toBeTruthy();
    }));

    it('should print the correct number', async(() => {
        const sayer = new NumberSayer();
        expect(sayer.sayIt(1)).toEqual('One');
        expect(sayer.sayIt(10)).toEqual('Ten');
        expect(sayer.sayIt(11)).toEqual('Eleven');
        expect(sayer.sayIt(19)).toEqual('Nineteen');
        expect(sayer.sayIt(20)).toEqual('Twenty');
        expect(sayer.sayIt(25)).toEqual('Twenty Five');
        expect(sayer.sayIt(125)).toEqual('One Hundred Twenty Five');
        expect(sayer.sayIt(1167)).toEqual('One Thousand One Hundred Sixty Seven');
        expect(sayer.sayIt(5603)).toEqual('Five Thousand Six Hundred Three');
        expect(sayer.sayIt(56107)).toEqual('Fifty Six Thousand One Hundred Seven');
        expect(sayer.sayIt(100000)).toEqual('One Hundred Thousand');
        expect(sayer.sayIt(223591)).toEqual('Two Hundred Twenty Three Thousand Five Hundred Ninety One');
        expect(sayer.sayIt(1000000)).toEqual('One Million');
        expect(sayer.sayIt(1000100)).toEqual('One Million One Hundred');
        expect(sayer.sayIt(1000000000)).toEqual('One Billion');
    }));
});
