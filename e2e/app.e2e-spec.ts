import { NumberSayerPage } from './app.po';

describe('number-talker App', () => {
  let page: NumberSayerPage;

  beforeEach(() => {
    page = new NumberSayerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
